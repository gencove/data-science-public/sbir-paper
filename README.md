# sbir-paper

Repo for figure prep, data analysis, and latex code for Li et al. 2020 "Low-pass sequencing increases the power of GWAS and decreases measurement error of polygenic risk scores compared to genotyping arrays".

`paper/*` contains the source TeX, tables, and figures needed for compilation of the manuscript. 

`notebooks/*` contains the code used to generate the statistics, tables, and figures present in the manuscript. 

`data/*` contains the raw data accessed and visualized by the code in `notebooks/*`.  